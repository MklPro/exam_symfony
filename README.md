
Pour pouvoir corriger le projet :

    1 : Clonez le projet sur votre ordinateur
    2 : Lancer composer install dans le terminal de votre IDE
    3 : Lancer yarn install dans le terminal de votre IDE
    4 : Créer un fichier .env.local et insérer cette ligne DATABASE_URL="mysql://root:@127.0.0.1:3306/exam_symfony?serverVersion=5.7&charset=utf8mb4
    5 : Lancer la commande symfony console doctrine:database:create dans le terminal de l'IDE
    6 : Lancer ensuite la commande symfony console make:migration puis symfony console doctrine:migrations:migrate toujours dans le terminal
    7 : Ouvrer ensuite wamp server et phpmyadmin
    8 : Créer quelques user dont un avec le role admin
    9 : Créer quelques forum et messages
    10 : Lancer un yarn watch dans votre terminal
    11 : Dans un autre terminal lancer un symfony serve
    12 : Vous pouvez désormais naviguer sur le projet et consulter / ajouter un message et ajouter un forum si vous êtes un admin !
