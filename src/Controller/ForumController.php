<?php

namespace App\Controller;

use App\Entity\Forum;
use App\Entity\Message;
use App\Form\AddForumType;
use App\Form\MessageType;
use App\Repository\ForumRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ForumController extends AbstractController
{
        private ForumRepository $forumRepository;

        /**
         * @param ForumRepository $forumRepository
         */
        public function __construct(ForumRepository $forumRepository)
        {
                $this->forumRepository = $forumRepository;
        }

        #[Route('/', name: 'forumList')]
        public function index(): Response
        {
                $forumEntities = $this->forumRepository->findAll();

                return $this->render('forum/index.html.twig', [
                          'forums' => $forumEntities,
                ]);
        }

        #[Route('/forum/{id}', name: 'forum')]
        public function forum($id, Request $request, EntityManagerInterface $entityManager): Response
        {
                $forumEntity = $this->forumRepository->find($id);
                $message = new Message();
                $messageForm = $this->createForm(MessageType::class, $message);
                $messageForm->handleRequest($request);

                if ($messageForm->isSubmitted() && $messageForm->isValid()) {
                        $message->setCreatedAt(new \DateTime());
                        $message->setUser($this->getUser());
                        $message->setForum($forumEntity);
                        $entityManager->persist($message);
                        $entityManager->flush();
                }

                return $this->renderForm('forum/forum.html.twig', [
                          'forum' => $forumEntity,
                          'form' => $messageForm
                ]);
        }

        #[Route('/admin/add_forum', name: 'forumAdd')]
        public function forumAdd(Request $request, EntityManagerInterface $entityManager): Response
        {
                $forum = new Forum();
                $forumForm = $this->createForm(AddForumType::class, $forum);
                $forumForm->handleRequest($request);

                if ($forumForm->isSubmitted() && $forumForm->isValid()) {
                        $forum->setCreatedAt(new \DateTime());
                        $forum->setUser($this->getUser());
                        $entityManager->persist($forum);
                        $entityManager->flush();

                        return $this->redirectToRoute('forumList');
                }

                return $this->renderForm('forum/forumAdd.html.twig', [
                          'form' => $forumForm
                ]);
        }

}
